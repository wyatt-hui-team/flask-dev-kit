from setuptools import find_packages, setup

setup(
    name='my_app',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask~=1.0',
        'python-dotenv~=0.10',
        'waitress~=1.2',
        'wheel~=0.33'
    ]
)
