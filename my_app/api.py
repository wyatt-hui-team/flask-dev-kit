from flask import Blueprint, jsonify
from .controllers import home_controller

BP = Blueprint('api', __name__, url_prefix='/api')

@BP.route('/', methods=['GET'])
def index():
    return jsonify(home_controller.index())
