### Dependencies
* Python 3.x
* pip3

### Installation
Create an environment
* /
    ```
    python3 -m venv venv
    ```

Activate the environment
* /

    Before you work on your project, activate the corresponding environment
    ```
    . venv/bin/activate
    ```

    Upgrade pip and setuptools
    ```
    pip3 install --upgrade pip
    pip3 install --upgrade setuptools
    ```

Installation for Python packages
* /

    Copy `.env.example` to `.env`
    ```
    pip3 install -e .
    ```

* /{app_name}

    Copy `.env.example` to `.env`

### Development
Run flask
* /
    ```
    flask run
    ```

### Deployment instructions
For version update, please edit the version number in
* setup.py

### Building for source
Prepare package
* /
    ```
    python3 setup.py bdist_wheel
    ```

* /dist

    Move `.whl` file to server

### Installation for source
* Install `.whl` file
    ```
    pip3 install {app_name}-{version_number}-py3-none-any.whl
    ```

* Setup `.env` in the app installation path

* Run app
    ```
    waitress-serve --call '{app_name}:create_app'
    ```

### Team contacts
* Somebody (somebody@example.com)

### License
Somebody
